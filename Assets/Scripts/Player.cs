﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {
	
	public Text healthText;
	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip fruitSound1;
	public AudioClip fruitSound2;
	public AudioClip sodaSound1;
	public AudioClip sodaSound2;
	public AudioClip gulp;
	public AudioClip bottlecap;
	public AudioClip radioSound1;
	public AudioClip radioSong1;
	public AudioClip radioSong2;
	public AudioClip radioSong3;
	public AudioClip radioSong4;
	public AudioClip radioSong5;
	public AudioClip radioSong6;

	
	private Animator animator;
	private int playerHealth;
	private int attackPower = 1;
	private int healthPerFruit = 5;
	private int healthPerSoda = 10;
	private int healthPerRadio = 2;
	private int healthPerCap = 25;
	private int healthPerNuka = 50;
	private int secondsUntilNextLevel = 1;
	
	protected override void Start()
	{
		base.Start();
		animator = GetComponent<Animator>();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = " " + playerHealth;
	}
	
	private void onDisalble()
	{
		GameController.Instance.playerCurrentHealth = playerHealth;
	}
	
	void Update () {
		
		if (!GameController.Instance.isPlayerTurn) 
		{
			return;
		}
		
		CheckIfGameOver ();
		
		int xAxis = 0;
		int yAxis = 0;
		
		xAxis = (int)Input.GetAxisRaw("Horizontal");
		yAxis = (int)Input.GetAxisRaw("Vertical");
		
		if (xAxis != 0) 
		{
			yAxis = 0;
		}
		
		if (xAxis != 0 || yAxis != 0) 
		{
			playerHealth--;
			healthText.text = " " + playerHealth;
			SoundController.Instance.PlaySingle(movementSound1, movementSound2);
			Move<Wall>(xAxis, yAxis);
			GameController.Instance.isPlayerTurn = false;
		}
		
	}
	
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if(objectPlayerCollidedWith.tag == "Exit")
		{
			Invoke("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}
		else if(objectPlayerCollidedWith.tag == "Fruit")
			
		{
			
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " \n" + " " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
		}
		else if(objectPlayerCollidedWith.tag == "Soda")
		{
			
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + " \n" + " " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
		}
		else if(objectPlayerCollidedWith.tag == "Radio")
		{
			
			playerHealth += healthPerRadio;
			healthText.text = "+" + healthPerRadio + " \n" + " " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(radioSound1, radioSong1, radioSong2, radioSong3, radioSong4, radioSong5, radioSong6);
		}
		else if(objectPlayerCollidedWith.tag == "Cap")
		{

			playerHealth += healthPerCap;
			healthText.text = "+" + healthPerCap + " \n" + " " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(bottlecap);
		}
		else if(objectPlayerCollidedWith.tag == "Nuka")
		{
			
			playerHealth += healthPerNuka;
			healthText.text = "+" + healthPerNuka + " \n" + " " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(gulp);
		}
	}
	
	private void LoadNewLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
	
	protected override void HandleCollision<T>(T component)
	{
		Wall wall = component as Wall;
		animator.SetTrigger("playerAttack");
		SoundController.Instance.PlaySingle (chopSound1, chopSound2);
		wall.DamageWall(attackPower);
		
	}
	
	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + " \n" + " " + playerHealth;
		animator.SetTrigger("playerHurt");
	}
	
	private void CheckIfGameOver()
	{
		if (playerHealth <= 0) {
			GameController.Instance.GameOver();
		}
	}
}







